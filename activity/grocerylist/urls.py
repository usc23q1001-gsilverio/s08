from django.urls import path
from . import views


# Syntax
# path(route, view, name)

urlpatterns = [
	path('', views.index, name='index'),
	#/todolist/<todoitem_id>
	#/todolist/1
	# The <int:todoitem_id> allows for creating a dynamic link where the todoitem_id is provided
	path('<int:groceryitem_id>/', views.groceryitem, name='viewgroceryitem'),
]