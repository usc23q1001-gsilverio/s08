from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
# from django.template import loader

from .models import ToDoItem

def index(request):
    todoitem_list = ToDoItem.objects.all()
    # template = loader.get_template("todolist/index.html")
    context = {'todoitem_list': todoitem_list}
    return render(request, "todolist/index.html", context)
    
    # output = ', '.join([todoitem.task_name for todoitem in todoitem_list])

    # return HttpResponse(output)


# def index(request):
#     return HttpResponse("Hello from the views.py file")

def todoitem(request, todoitem_id):
    response = "You are viewing the details of %s"
    return HttpResponse(response % todoitem_id)